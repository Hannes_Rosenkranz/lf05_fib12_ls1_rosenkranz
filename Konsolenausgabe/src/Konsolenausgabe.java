
public class Konsolenausgabe {
	
	public static void main(String [] args) {
		
		
		String b1 = "Das ist ein \"Beispielsatz\". \n";
		String b2 = "Ein Beispielsatz ist das.";
		System.out.println("Aufgabe 1:\n");
		System.out.println(b1 + b2 );
		System.out.println(); 								//Leertaste
		
		//Aufgabe 2
		System.out.println("Aufgabe 2:\n");
		
		String s1 = ("*");
		String s2 = ("***");
		String s3 = ("*****");
		String s4 = ("*******");
		String s5 = ("*********");
		String s6 = ("***********");
		String s7 = ("*************");
		String s8 = ("***");
		
		System.out.printf("%7s\n", s1);
		System.out.printf("%8s\n", s2);
		System.out.printf("%9s\n", s3);
		System.out.printf("%10s\n", s4);
		System.out.printf("%11s\n", s5);
		System.out.printf("%12s\n", s6);
		System.out.printf("%13s\n", s7);
		System.out.printf("%8s\n", s8);
		System.out.printf("%8s\n", s8);
		System.out.println();
		
		// Aufgabe 3
		System.out.println("Aufgabe 3:\n");
		
		double d1 = 22.4234234;
		double d2 = 111.2222;
		double d3 = 4.0;
		double d4 = 1000000.551;
		double d5 = 97.34;
		
		System.out.printf( "%.2f \n" , d1);	
		System.out.printf( "%.2f \n" , d2);	
		System.out.printf( "%.2f \n" , d3);	
		System.out.printf( "%.2f \n" , d4);	
		System.out.printf( "%.2f \n" , d5);	
		System.out.println();
		//test




		
	}

}
