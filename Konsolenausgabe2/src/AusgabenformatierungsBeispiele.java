
public class AusgabenformatierungsBeispiele {
	
	public static void main (String[] args) {
		
		//Ganzzahl
		System.out.printf("|%+-10d%+10d]\n", 123456, -123456);
		
		//Kommazahlen
		System.out.printf("|%+-10.2f|", 12.1234556473);
		
	}

}
