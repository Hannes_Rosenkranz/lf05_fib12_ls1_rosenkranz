import java.util.Arrays;
import java.util.Scanner;

public class einfacheArrays {
	
	public static void main(String[] args) {
		
		System.out.println(Arrays.toString(aufgabe1()));
		System.out.println(Arrays.toString(aufgabe2()));
		System.out.println(Arrays.toString(aufgabe3()));
		aufgabe4();

	}
	
	
	
	public static int[] aufgabe1(){
		
		int[] numbers = new int[10];
		
		for(int i = 0; i < numbers.length; i++) {
			numbers[i] = i;
		}
		
		return numbers;		
	}
	
	public static int[] aufgabe2() {
		
		int[] ungeradeZahlen = new int[10];
		int counter = 1;
		
		for(int i = 0; i < ungeradeZahlen.length; i++) {
			ungeradeZahlen[i] = counter;
			counter += 2;		
		}
		
		return ungeradeZahlen;
	}
	
	public static int[] aufgabe3() {
		
		Scanner sc = new Scanner(System.in);
		int[] reverse = new int[5];
		
		for(int i = 4; i >= 0; i--) {
			System.out.println("Bitte jeweils eine Zahl eingeben. ");
			reverse[i] = sc.nextInt();
		}
		
		return reverse;						
	}
	
	public static void aufgabe4() {
		
		int[] lotto = {3, 7, 12, 18, 37, 42};
		boolean isTwelve = false;
		boolean isThirteen = false;
		
		
		for(int i = 0; i < lotto.length; i++) {
			
			if(lotto[i] == 12) {
				isTwelve = true;
			}else if(lotto[i] == 13){
				isThirteen = true;
			}
			
			switch (i) {
			case 0:
				System.out.print("[ " + lotto[i] + " ");
				break;
			case 5:
				System.out.print(lotto[i] + " ]\n");
				break;

			default:
				System.out.print(lotto[i] + " ");
			}		
		}
				
		
		if(isTwelve) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");		
		}
		else {
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		}
		
		if(isThirteen) {
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");		
		}
		else {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
					
	}

}
