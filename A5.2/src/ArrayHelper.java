import java.util.Arrays;



public class ArrayHelper {
	
	public static void main(String[] args) {
		int[] zahlen = {1,2,3,4,5,6,7,8};
		int[] zahlenTwo = {1,2,3,4,5,6,7,8};
		System.out.println(convertArrayToString(zahlen));
		System.out.println(Arrays.toString(reverseIntArray(zahlen)));
		System.out.println(Arrays.toString(reverseIntArrayAlt(zahlenTwo)));
		System.out.println(Arrays.deepToString(temperature(5)));
		System.out.println(Arrays.deepToString(matrix(5, 3)));
		
		int[][] a = {
			      		{1, 2, 3}, 
			      		{4, 5, 6, 9}, 
			      		{7}, 
					};
		
		int[][] b = {
			      		{1, 3}, 
			      		{3, 1}, 
					};
		System.out.println(transponiert(a));
		System.out.println(transponiert(b));
		
		
	}
	
	
	//Aufgabe 1
	public static String convertArrayToString(int[] zahlen) {
		
		String ausgabe = "";
		
		for(int i = 0; i < zahlen.length; i++) {
			
			if(i == zahlen.length-1) {
				ausgabe += String.valueOf(zahlen[i]);
			}else {
				ausgabe += String.valueOf(zahlen[i]) + ", ";
			}
		}
		
		return ausgabe;
	}
	
	//Aufgabe 2
	public static int[] reverseIntArray(int[] zahlen) {
		
 		int tmp = 0;
 		
		for(int i = 0; i < zahlen.length/2; i++) {
		
			tmp = zahlen[i];
			zahlen[i] = zahlen[zahlen.length - 1 - i];
			zahlen[zahlen.length - 1 - i] = tmp;
			
		}
		
		return zahlen;
	}
	
	//Aufgabe 3
	public static int[] reverseIntArrayAlt(int[] zahlen) {
		
		int[] zahlenAusgabe = new int[zahlen.length];
		
		for(int i = 0; i < zahlen.length; i++) {
			
			zahlenAusgabe[i] = zahlen[zahlen.length - 1 - i];
		}
		
		return zahlenAusgabe;	
	}
	
	//Aufgabe 4
	public static double[][] temperature(int n) {
		
		double[][] temperArr = new double[2][n];
		
		for(int i = 0; i < n; i++) {
			temperArr[0][i] = i * 10.0;
			temperArr[1][i] = (5.0 / 9.0) * ((i * 10.0) - 32);			
		}
		
		return temperArr;	
	}
	
	//Aufgabe 5
	public static int[][] matrix(int n, int m) {

        int[][] matrix = new int[n][m];

        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                matrix [i][j] = i;
            }
        }

        return matrix;
    }
	
	
	//Aufgabe 6
	public static boolean transponiert(int[][] matrix) {
		
		boolean trans = false;
		
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[0].length; j++) {
				if(matrix[i][j] == matrix[j][i]) {
					trans = true;
				}else {
					trans = false;
					break;
				}
				
			}
		}
		return trans;
	}
	
}
