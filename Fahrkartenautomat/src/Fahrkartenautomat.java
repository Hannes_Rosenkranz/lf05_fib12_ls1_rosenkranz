﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
    	
    	
    	//A4.5 2. Aufgabe
    	while(true) {
    		
    		 double zuZahlenderBetrag = fahrkartenbestellungErfassen(); 
    	     double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	     fahrkartenAusgeben();
    	     rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
    	}
    	 	
      
    }
        
       
    
    public static double fahrkartenbestellungErfassen() {
       
    	Scanner tastatur = new Scanner(System.in);
    	
    	final double EINZELFAHRSCHEIN = 2.90;
        final double TAGESKARTE = 8.60;
        final double KLEINGRUPPENTAGESKARTE = 23.50;
        
        double einzelPreis = 0.0;
        int auswahl;
        int anzahlTickets;
        
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println("   Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + 
        				   "   Tageskarte Regeltarif AB [8,60 EUR] (2)\n" + 
        				   "   Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
        
        auswahl = tastatur.nextInt();
        
        while(auswahl > 3 || auswahl < 1) {
        	System.out.println(">>falsche Eingabe<<");
        	auswahl = tastatur.nextInt();
        } 
        	 
        switch(auswahl) {
            case 1:
            	 
            	einzelPreis = EINZELFAHRSCHEIN;
            	break;
            	 
            case 2:
            	 
            	einzelPreis = TAGESKARTE;           	 
             	break;
             	 
            case 3:
            	 
            	einzelPreis = KLEINGRUPPENTAGESKARTE;
             	break;
        }   	
        	
        
        
        System.out.println("Anzahl Tickets: ");
        anzahlTickets = tastatur.nextInt();

             
        if(anzahlTickets < 1 || anzahlTickets > 10) {
        	
        	System.out.println("Fehlerhafte Eingabe \ndie Anzahl der Tickets wird auf 1 gesetzt.");
        	anzahlTickets = 1;
        }
        
         return anzahlTickets*einzelPreis;
            	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
       
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: " + "%.2f Euro \n" ,(zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	return eingezahlterGesamtbetrag;	
    }
    
    public static void fahrkartenAusgeben() {
    	    	
    	System.out.println("\nFahrschein wird ausgegeben");

         
    	for (int i = 0; i < 8; i++)
  	   	{
  		   System.out.print("=");
  		   warte(250);		   
  	   }
         System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f EURO \n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  muenzenAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  muenzenAusgeben(1 ,"EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  muenzenAusgeben(50, "CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  muenzenAusgeben(20, "CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  muenzenAusgeben(10, "CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  muenzenAusgeben(5, "CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n"); 	
    }
    
    public static void warte(int milisekunde) {
    	try {
			   Thread.sleep(milisekunde);
		   } catch (InterruptedException e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }   	
    }
    
    public static void muenzenAusgeben(int betrag, String einheit) {
   	  System.out.println(betrag + einheit);
    }
    
    
}




/*
5.) Ich habe als Datentyp für die Anzahl der Fahrkarten Integer gewählt da hier nur ganzzahlen benötigt werden
	denn es gibt keine 1,5 usw. Karten
	
6.) Bei der Berechnung von anzahl*einzelpreis wird die eingegebene anzahl auf den zuZahlenden Betrag gerechnet.
	Dieser wird dann im laufe des Programmes weiter übernommen.
	
	Bezüglich der Rechnung bedeutet das als Beispiel: 
	einzelPreis: 5 Euro (Anzahl einer Karte) Double
	anzahlTickets: 2 (Anzahl der Tickets) Integer
	-> 2.0 Tickets * 5.0 Euro = 10.0 zu Zahlen
	Hier wird Integer zu Double umgewandelt damit keine Typkonvertierungserrors entstehen
	
	
*/