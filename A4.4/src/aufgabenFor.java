import java.util.Scanner;

public class aufgabenFor {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		int number = sc.nextInt();
		zaehlen(number);
		
		int sum = sum(number);
		System.out.println("-> Die Summe von 1-" + number + " ist: " + sum);
		
		int modNumber = 200;
		System.out.print("-> Alle Zahlen bis 200 die durch 7 und 4 teilbar sind, aber nicht durch 5: ");
		modulo(200);
		
		System.out.println("\n");
		System.out.println("Es folgt das Einmal Eins: ");
		einmalEins();
		
		System.out.println();
		stars(number);
		
		System.out.println();
		System.out.println("Zahlenfolgen:");
		folgenAufgabe();
		
		
		
		
	}
	
	
	
	public static void zaehlen(int n) {
		
		String aufsteigend = "";
		String absteigend = "";
		
		char j = Integer.toString(n).charAt(0);
		
		for(int i = 1; i <= n; i++) {
			aufsteigend += Integer.toString(i).charAt(0);
			aufsteigend += " ";
			absteigend += j;
			absteigend += " ";
			
			j--;
		}
		
		System.out.println("-> aufsteigend: " + aufsteigend);
		System.out.println("-> absteigend: " +  absteigend);
	}
	
	public static int sum(int n) {
		int sum = 0;
		
		for(int i = 1; i <= n; i++) {
			sum += i;
		}
		
		return sum;
	}
	
	public static void modulo(int n) {
			
		for(int i = 1; i < n; i++) {
			if(i % 7 == 0 && i % 5 != 0 && i % 4 == 0) {
				System.out.print(i + " ");
			}
		}
	}
	
	public static void einmalEins() {
		String topRow = "    1  2  3  4  5  6  7  8  9 10";
		System.out.println(topRow);
		int multiply = 1;
		
		for(int i = 1; i <= 10; i++) {
			System.out.printf("%2d ", i);
			for(int j = 1; j <= 10; j++) {
				multiply = i * j;
				System.out.printf("%2d ", multiply);
				if(j == 10) {
					System.out.println();
				}
			}
		}		
	}
	
	public static void stars(int n) {
		for(int i = 1; i <= n; i++) {
			for(int j = 1; j <= i; j++) {
			System.out.print("*");
			if (j == i) {
				System.out.println();
			}
		  }
		}
	}
	
	public static void folgenAufgabe() {
		
		//a 
		for(int i = 99; i >= 12; i -= 3) {
			System.out.print(i + ", ");
			
			if(i == 12) {
				i -= 3;
				System.out.print(i + "\n");
			}
		}
		
		//c
		for(int i = 2; i <= 102; i+= 4) {
			System.out.print(i + ", ");
			
			if(i == 98) {
				i+= 4;
				System.out.print(i + "\n");
			}
			
		}
		
		//d
		int power;
		for(int i = 1; i <= 32; i++) {
			power = (int) Math.pow(i, 2);
			System.out.print(power + ", ");
			
			if(i == 32) {
				System.out.print(power + "\n");
			}
		}
	}
	


}
