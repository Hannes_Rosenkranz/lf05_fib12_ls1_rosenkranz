import java.util.ArrayList;
import java.util.List;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieVersorgungInProzent;
	private int schildeInProzent;
	private int hülleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffname;
	// wir benutzen in diesem Falle die superclass List von Arraylist welche im
	// Konstruktor auf ArrayList gesetzt wird
	private List<String> broadcastKommunikator;
	private List<Ladung> ladungsverzeichnis;

	public Raumschiff() {

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieVersorgungInProzent, int schildeInProzent,
			int hülleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffname) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieVersorgungInProzent = energieVersorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.hülleInProzent = hülleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffname = schiffname;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}

	public void addLadung(Ladung neueLadung) {
		for (Ladung ladung : ladungsverzeichnis) {
			if (ladung.getBezeichnung() == neueLadung.getBezeichnung()) {
				ladung.setMenge(ladung.getMenge() + neueLadung.getMenge());
				return;
			}
		}
		ladungsverzeichnis.add(neueLadung);
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}


	public int getEnergieVersorgungInProzent() {
		return energieVersorgungInProzent;
	}


	public void setEnergieVersorgungInProzent(int energieVersorgungInProzent) {
		this.energieVersorgungInProzent = energieVersorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}


	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}


	public int getHülleInProzent() {
		return hülleInProzent;
	}


	public void setHülleInProzent(int hülleInProzent) {
		this.hülleInProzent = hülleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffname() {
		return schiffname;
	}

	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}


	public List<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}


	public void setBroadcastKommunikator(List<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public List<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(List<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}


}
