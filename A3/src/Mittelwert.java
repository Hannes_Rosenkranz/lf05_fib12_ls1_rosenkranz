import java.security.DrbgParameters.NextBytes;
import java.util.Scanner;

public class Mittelwert {
	
	   public static void main(String[] args) {

		   inputMittelwert();	      	      		      
	   }
	   
	   public static double berechneMittelwert(double x, double y) {
		   return (x + y) / 2.0;

	   }
	   
	   public static void inputMittelwert() {
		  
		   double x;
		   double y; 
		   double m;
		   char input;
		      
		   Scanner sc = new Scanner(System.in);
		   boolean weiter = true;
		      
		   while(weiter){
			   
			   System.out.println("Bitte geben sie die erste Zahl ein");	    	  
			   x = sc.nextDouble();
		    	  
			   System.out.println("Bitte geben sie die zweite Zahl ein");
			   y = sc.nextDouble();
		    	  
			   m = berechneMittelwert(x, y);
			      
			   System.out.println();
			   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
			   System.out.println();
			      
			   System.out.println("Möchten sie einen weiteren Mittelwert bestimmen? j/n");
			   input = sc.next().charAt(0);
			      
			   if(input == 'n') {
				   weiter = false;
			   }       
		     }
	   }
}
