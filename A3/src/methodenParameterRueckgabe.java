import java.security.DrbgParameters.NextBytes;
import java.util.Scanner;

public class methodenParameterRueckgabe {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("r1 angeben: ");
		double r1 = sc.nextDouble();
		
		System.out.println("r2 angeben: ");
		double r2 = sc.nextDouble();
		
		System.out.println("Reihenschaltung: " + reihenschaltung(r1, r2));
		
		System.out.println("Parallelschaltung: " + parallelschaltung(r1, r2));
	}
	
	
	//Aufgabe 3
	public static double reihenschaltung(double r1, double r2) {
		return r1+r2;
	}
	
	//Aufgabe 4
	public static double parallelschaltung(double r1, double r2) {
		return (r1*r2)/(r1+r2);
	}
	

}
