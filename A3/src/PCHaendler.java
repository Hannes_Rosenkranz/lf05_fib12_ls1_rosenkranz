
import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		//Daten auslesen
		String artikel = liesString("Was möchten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		//Rechnung
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		//Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
	}
	
	public static String liesString(String text) {
		Scanner scn = new Scanner(System.in);

		System.out.println(text);
		return scn.next();
		
	}
	
	public static int liesInt(String text) {
		Scanner scn = new Scanner(System.in);
		 
		System.out.println(text);
		return scn.nextInt();
		
	 }
	
	public static double liesDouble(String text) {
		Scanner scn = new Scanner(System.in);

		System.out.println(text);
		return scn.nextDouble();
				  
	  }
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		 return anzahl * nettopreis;
	 }
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
		 
	 
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, 
														double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%"); 
	 }
	
}