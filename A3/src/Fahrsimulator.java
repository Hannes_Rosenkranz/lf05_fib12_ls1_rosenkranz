import java.util.Scanner;

public class Fahrsimulator {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Bitte geschwindigkeit angeben -> ");
		double dv = sc.nextDouble();
		
		double v = beschleunige(0, dv);
		System.out.println("Die neue Geschwindigkeit lautet: " + v);

		
		
		
		while(carStopped(v) == false) {
			System.out.print("Bitte Geschwindigkeit angeben -> ");
			 dv = sc.nextDouble();
			 v = beschleunige(v, dv);
			 System.out.println("Die neue Geschwindigkeit lautet: " + v);

				
		}
		
		System.out.println("Das Auto hat angehalten!");
	
		
		
		
	}
	
	public static double beschleunige(double v, double dv) {
		
		if((v+dv) > 130) {
			v = 130;
		} else if((v+dv) < 0) {
			v = 0;
			
		} else {
			v += dv;
		}
				
		return v;
	
		
	}
	
	public static boolean carStopped(double v) {
		boolean stopped = false;
		
		if(v == 0) {
			stopped = true;
		}
		return stopped;
	}

}
