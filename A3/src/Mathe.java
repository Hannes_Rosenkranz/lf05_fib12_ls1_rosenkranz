import java.util.Scanner;

public class Mathe {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte eine Zahl zum quadrieren eingeben: ");
		double number = sc.nextDouble();
		System.out.println(number + "^2 = " + quadrat(number));
		
		System.out.println("Bitte zwei Zahlen zur berechnung der Hypotenuse angeben: ");
		number = sc.nextDouble();
		double number2 = sc.nextDouble();
		System.out.println("hyp(" + number + ", " + number2 +") = " + hypotenuse(number, number));
	}
	
	//Aufgabe 5
	public static double quadrat(double x) {
		return x*x;
	}
	
	//Aufgabe 7
	public static double hypotenuse(double kathete1, double kathete2) {
		
		return Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
	}
}
