import java.util.Iterator;
import java.util.Scanner;

public class AufgabeZuSchleifen {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bitte die größte Zahl der Reihe angeben");
		int n = scanner.nextInt();
		
		System.out.println("Die summe der Folge 1-" + n + " ist gleich : " + sum(n));
		System.out.println("Die summe der geraden Zahlen bis " + 2*n + " ist gleich: " + sumEven(n));
		System.out.println("Die summe der ungerade Zahlen bis " + (2*n-1) + " ist gleich: " + sumUneven(n));
		
	}

	
	public static int sum(int n) {
		
		int sum = 0;
		
		for(int i = 1; i <= n; i++) {
			sum += i;
		}
		
		return sum;
	}
	
	public static int sumEven(int n) {
		
		int sum = 0;
		
		for(int i = 2; i <= 2*n; i++) {
			if(i % 2 == 0) {
				sum += i;
			}
		}
		
		return sum;		
	}
	
	public static int sumUneven(int n) {
		
		int sum = 0;
		
		for (int i = 1; i <= (2*n+1); i++) {
			if(i % 2 != 0) {
				sum += i;
			}
			
		}
		
		return sum;
	}

}
